<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'users_id',
        'products_id'
    ];

    protected function product()
    {
        return $this->hasOne(Product::class, 'products_id');
    }

    protected function user(){
        return $this->belongsTo(User::class, 'users_id');
    }
}
